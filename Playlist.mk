##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Playlist
ConfigurationName      :=Debug
WorkspacePath          := "/home/etudiant/ppe3_project_nl_ms_cb"
ProjectPath            := "/home/etudiant/ppe3_project_nl_ms_cb"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=
Date                   :=09/28/16
CodeLitePath           :="/home/etudiant/.codelite"
LinkerName             :=/usr/bin/g++ 
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Playlist.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++ 
CC       := /usr/bin/gcc 
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as 


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/M3U.cpp$(ObjectSuffix) $(IntermediateDirectory)/XSPF.cpp$(ObjectSuffix) $(IntermediateDirectory)/album.cpp$(ObjectSuffix) $(IntermediateDirectory)/artiste.cpp$(ObjectSuffix) $(IntermediateDirectory)/format.cpp$(ObjectSuffix) $(IntermediateDirectory)/genre.cpp$(ObjectSuffix) $(IntermediateDirectory)/playlist.cpp$(ObjectSuffix) $(IntermediateDirectory)/morceau.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/M3U.cpp$(ObjectSuffix): M3U.cpp $(IntermediateDirectory)/M3U.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/M3U.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/M3U.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/M3U.cpp$(DependSuffix): M3U.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/M3U.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/M3U.cpp$(DependSuffix) -MM "M3U.cpp"

$(IntermediateDirectory)/M3U.cpp$(PreprocessSuffix): M3U.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/M3U.cpp$(PreprocessSuffix) "M3U.cpp"

$(IntermediateDirectory)/XSPF.cpp$(ObjectSuffix): XSPF.cpp $(IntermediateDirectory)/XSPF.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/XSPF.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/XSPF.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/XSPF.cpp$(DependSuffix): XSPF.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/XSPF.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/XSPF.cpp$(DependSuffix) -MM "XSPF.cpp"

$(IntermediateDirectory)/XSPF.cpp$(PreprocessSuffix): XSPF.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/XSPF.cpp$(PreprocessSuffix) "XSPF.cpp"

$(IntermediateDirectory)/album.cpp$(ObjectSuffix): album.cpp $(IntermediateDirectory)/album.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/album.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/album.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/album.cpp$(DependSuffix): album.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/album.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/album.cpp$(DependSuffix) -MM "album.cpp"

$(IntermediateDirectory)/album.cpp$(PreprocessSuffix): album.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/album.cpp$(PreprocessSuffix) "album.cpp"

$(IntermediateDirectory)/artiste.cpp$(ObjectSuffix): artiste.cpp $(IntermediateDirectory)/artiste.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/artiste.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/artiste.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/artiste.cpp$(DependSuffix): artiste.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/artiste.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/artiste.cpp$(DependSuffix) -MM "artiste.cpp"

$(IntermediateDirectory)/artiste.cpp$(PreprocessSuffix): artiste.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/artiste.cpp$(PreprocessSuffix) "artiste.cpp"

$(IntermediateDirectory)/format.cpp$(ObjectSuffix): format.cpp $(IntermediateDirectory)/format.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/format.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/format.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/format.cpp$(DependSuffix): format.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/format.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/format.cpp$(DependSuffix) -MM "format.cpp"

$(IntermediateDirectory)/format.cpp$(PreprocessSuffix): format.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/format.cpp$(PreprocessSuffix) "format.cpp"

$(IntermediateDirectory)/genre.cpp$(ObjectSuffix): genre.cpp $(IntermediateDirectory)/genre.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/genre.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/genre.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/genre.cpp$(DependSuffix): genre.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/genre.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/genre.cpp$(DependSuffix) -MM "genre.cpp"

$(IntermediateDirectory)/genre.cpp$(PreprocessSuffix): genre.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/genre.cpp$(PreprocessSuffix) "genre.cpp"

$(IntermediateDirectory)/playlist.cpp$(ObjectSuffix): playlist.cpp $(IntermediateDirectory)/playlist.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/playlist.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/playlist.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/playlist.cpp$(DependSuffix): playlist.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/playlist.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/playlist.cpp$(DependSuffix) -MM "playlist.cpp"

$(IntermediateDirectory)/playlist.cpp$(PreprocessSuffix): playlist.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/playlist.cpp$(PreprocessSuffix) "playlist.cpp"

$(IntermediateDirectory)/morceau.cpp$(ObjectSuffix): morceau.cpp $(IntermediateDirectory)/morceau.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/etudiant/ppe3_project_nl_ms_cb/morceau.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/morceau.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/morceau.cpp$(DependSuffix): morceau.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/morceau.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/morceau.cpp$(DependSuffix) -MM "morceau.cpp"

$(IntermediateDirectory)/morceau.cpp$(PreprocessSuffix): morceau.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/morceau.cpp$(PreprocessSuffix) "morceau.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) ./Debug/*$(ObjectSuffix)
	$(RM) ./Debug/*$(DependSuffix)
	$(RM) $(OutputFile)
	$(RM) ".build-debug/Playlist"


