#ifndef MORCEAU_H
#define MORCEAU_H
#include <string>
class morceau
{
public:
	morceau();
	~morceau();
private:
unsigned int id;
std::string title;
std::string artist;
std::string album;
unsigned int duration;

public:
unsigned int Getid();
std::string Gettitle();
std::string Getartist();
std::string Getalbum();
unsigned int Getduration();
};

unsigned int morceau::Getid() {
	return id;
}
std::string morceau::Gettitle() {
	return title;
}
std::string morceau::Getartist() {
	return artist;
}

std::string morceau::Getalbum() {
	return album;
}

unsigned int morceau::Getduration() {
	return duration;
}

#endif // MORCEAU_H
