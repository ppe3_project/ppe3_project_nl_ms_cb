#ifndef PLAYLIST_H
#define PLAYLIST_H
#include <string>
class playlist
{
public:
	playlist();
	~playlist();
	void add();
	void Delete();
	void rename();
	void organize();

private:
	unsigned int id;
	unsigned int duration;
	std::string features;

	
public:
unsigned int Getid();
unsigned int Getduration();
std::string Getfeatures();
};

unsigned int playlist::Getid() {
	return id;
}
unsigned int playlist::Getduration() {
	return duration;
}
std::string playlist::Getfeatures() {
	return features;
}
#endif // PLAYLIST_H
