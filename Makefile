.PHONY: clean All

All:
	@echo "----------Building project:[ Playlist - Debug ]----------"
	@$(MAKE) -f  "Playlist.mk"
clean:
	@echo "----------Cleaning project:[ Playlist - Debug ]----------"
	@$(MAKE) -f  "Playlist.mk" clean
